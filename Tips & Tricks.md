# Tips & Tricks

---

Here you will find random tips and tricks (notes, really) because it isn't very
smart to not store possibly useful knowledge right after obtaining such
knowledge.

In other words, the dev's notes.


## Going as fast as possible

---

If you want to run your compiled bf as fast as possible, there are a few things
you might want to consider:

**1)** Compiling without wrapping cells ("__`-c 0`__")  
While it might seem wierd as to why this will make your code run faster, it does
simply becuase every addition or subtraction command will also check if it will
need to wrap - in other words, doing a modulo operation every addition or
subtraction operation.

**2)** Using more gcc flags than provided  
The dev is no gcc compilation flag wizard, so an actual wizard will probably be
able to make any code run faster than what is provided by default.

**And last, but not least:** Complain to the dev to optimise more.  
This one is not always guranteed to work, and when it does it will take time,
but if it ever works, it may provide potentially large speedups.
