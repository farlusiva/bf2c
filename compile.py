#!/usr/bin/env python3
from optparse import OptionParser
import re


usage="usage: %prog file [options]"
parser = OptionParser(usage=usage)

parser.add_option(
	"--debug",
	action="store_true",
	dest="debug",
	help="debug mode",
	default=False,
	)

parser.add_option(
	"-o", "--output",
	type="string",
	dest="output",
	default="output.c",
	metavar="FILE",
	help="name of output file (default output.c)"
	)

parser.add_option(
	"-c", "--cellsize",
	type="int",
	dest="cell_size",
	default=8,
	help="Cell size in bits, defaults to 8 (0 for the maximum)"
	)

parser.add_option(
	"-f", "--slow",
	dest="optimise",
	default=True,
	action="store_false",
	help="Don't optimise the compiled code"
	)

parser.add_option(
	"-C", "--cells",
	type="int",
	dest="cellcount",
	default=65536,
	help="Amount of cells (default 65536)"
	)



(options, args) = parser.parse_args()


if len(args) == 0:
	print("No input given")
	raise SystemExit


else:
	options.filename = args[0]


# Approved cell sizes, giving free
# modulo when you over/underflow (not tested)
approved_cellsizes = (8, 16, 32, 64)

if options.cell_size == 0:
	# no modulo, maximum width
	inttype = "uintmax_t"

elif options.cell_size in approved_cellsizes:
	# no modulo as the datatype handles it
	inttype = f"uint{options.cell_size}_t"

else:
	print("The cell size you're asking for is not ")
	print("a data type. Try using one of the following:")
	print(approved_cellsizes)
	raise SystemExit


if options.debug:
	print(f"Int type: {inttype}")


# The files
to_read = str(open(options.filename, "r").read())
to_write = open(options.output, "w")

if options.debug:
	print(f"Reading from {options.filename}")
	print(f"Writing to {options.output}")




# Boilerplate

# pos = position in code (pointer position)
# tape = the actual tape

begin_boilerplate = f"""\
#include <stdio.h>
#include <inttypes.h>

int pos = 0;
{inttype} tape [{options.cellcount}];

int main() {{

"""
to_write.write(begin_boilerplate)

il = 1 # il = indentation level
indent = "\t"


if options.debug:
	print("Stripping code")

read = ""
for k in to_read:

	if k in "+-<>[],.":
		read += k


# Close to_read and replace it with read
to_read = read


if options.debug:
	print("Compiling...")
	print(f"Length of program: {len(to_read)}\n")


length_of_program = len(to_read)

i = 0
while i < length_of_program:
	k = to_read[i]

	# Code for optimising the output
	if options.optimise:


		# Find out how long the same character goes on in repetition
		temp = i
		counter = 0
		while to_read[i] == to_read[temp]:
			counter += 1
			temp += 1

			if temp > len(to_read) - 2:
				break


		# Optimise "[-]" to just set tape[pos] to 0.
		if to_read[i : i + 3] == "[-]":
			to_write.write(f"{indent*il}tape[pos] = 0;\n")
			i += 3
			continue


		# A hacked together, stupid way of optimising copy/multiplication loops.
		# Well well, it's the first thing I came up with, and I
		# haven't got any other ideas on how to do it.

		# We only need to test for a copyloop if our current character will
		# start a loop

		# Fallbacks
		tmp = None
		good = False

		## Because python is stupid in not allowing us to break from if-statements,
		## we have to have 3 if-statements in order to have some breakpoints
		## so that at times when we know that we aren't at a copyloop, that
		## it'll test for the copyloop (basically a theoretical optimisation)

		if k == "[":
			# Here we have the regex that will only match if we have a copyloop
			tmp = re.match("^\[[+-<>]*\]", to_read[i:])
			# The "^" at the beginning is to ensure that we know it'll match
			# from where we are and not some upcoming copyloop that we'll
			# test for later



		if tmp is not None:
			regx = to_read[i : i + tmp.span()[1]]

			## More specific copyloop testing

			match1 = re.match("^\[[+-<>]*\]$", regx)
			match2 = re.match("^\[[<>]*\]$", regx)
			# "Move-loop", for example "[<<<]". *NOT* a copyloop
			bool1 = regx.count("<") == regx.count(">")
			# This copy-loop will *only* work if the position won't
			# move at all


			# Matches match1, does not mach match2 and bool1 is true
			good = (match1 != None) and (match2 == None) and bool1


			# A second test, to make sure everything is right.
			# Could be joined into one line, but that'd be too long.
			if regx.count("[") > 1 or regx.count("]") > 1 or regx.count(".") > 0 or regx.count(",") > 0:
				good = False


			# Main copyloop creation code

			# How it works is as follows:
			# It sets up a fake bf enviorment (basically a chroot), then
			# it'll run the copyloop *once*. From thereon, it'll record the changes
			# done and turn that into some code.
			if good:

				# Setup the fake enviorment

				fk_tape = [0] * options.cellcount * 2

				# pos here *must* have at least *some* space before, so that
				# the copyloop can also copy/multiply things before tape[pos]
				# too...

				# The amount of cells is also multiplied by two, so that the
				# pointer could technically go as far in any direction as the
				# normal tape is.

				pos = options.cellcount # Right in the middle
				# One limitation of this program would be that the compilation
				# wouldn't work properly if a copyloop would try to copy to
				# something out of bounds for pos

				# fk_tape[offset] is basically fk_tape[0] as bf in concerned
				offset = pos

				## Simple brainfuck interpreter

				# cc = current charcter
				for cc in regx:

					if   cc == "+": fk_tape[pos] += 1
					elif cc == "-": fk_tape[pos] -= 1

					elif cc == ">": pos += 1
					elif cc == "<": pos -= 1

				# Make sure we're decrementing by one and only one
				if fk_tape[offset] != -1:
					good = False


			# Here we write the changes
			if good:

				if options.debug:
					print("\nCopyloop", regx)
					#print(fk_tape)

				for m, tmp in enumerate(fk_tape): # fk_tape[m] == tmp


					# We can't reduce m *before* doing the copyloop things
					if tmp != 0 and m != offset:

						# There is some ugly inline boolean multiplication here
						# so tmp only gets multiplied with tape[pos] if it isn't
						# 1.

						# Technically not needed, and the performance boost it gives
						# is probably nothing; but hey, it makes the code no one
						# looks at prettier.
						to_write.write(f"{indent*il}tape[pos+{m-offset}] += {((str(tmp) + '*') * (tmp != 1))}tape[pos];\n")


				# Copyloops always end up with tape[pos] at 0.
				to_write.write(f"{indent*il}tape[pos] = 0;\n")


				i += len(regx)
				continue





	# Set counter to 1 when not optimising, basically disabling the optimising
	# when it comes to bunching together multiple of the same movement/cell commands
	else:
		counter = 1



	if k == "+":
		to_write.write(f"{indent*il}tape[pos] += {counter};\n")
		i += counter - 1

	elif k == "-":
		to_write.write(f"{indent*il}tape[pos] -= {counter};\n")
		i += counter - 1

	elif k == "<":
		to_write.write(f"{indent*il}pos -= {counter};\n")
		i += counter - 1

	elif k == ">":
		to_write.write(f"{indent*il}pos += {counter};\n")
		i += counter - 1

	elif k == "[":
		to_write.write(f"{indent*il}while (tape[pos] != 0) {{\n")
		il += 1

	elif k == "]":
		il -= 1
		to_write.write(f"{indent*il}}}\n")

	elif k == ".":
		to_write.write(f"{indent*il}printf(\"%c\", tape[pos]);\n")

	elif k == ",":
		to_write.write(f"{indent*il}printf(\"The program asks for input: \");\n")
		to_write.write(f"{indent*il}scanf(\" %c\", tape[pos]);\n")

	i += 1

	if i == len(to_read):
		break





# Return 0 and end main
end_boilerplate = f"""
{indent}return 0;
}}\n"""


to_write.write(end_boilerplate)
