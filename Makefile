CC = cc
CFLAGS = -std=c99 -pedantic -O3 -march=native -pipe
BINFILE = output
COUT = output.c

.PHONY: all
all: output

output:

	./compile.py $(input) $(args)
	$(CC) $(CFLAGS) $(COUT) -o $(BINFILE)
	./$(BINFILE)


.PHONY: clean
clean:
	rm -f $(BINFILE) $(COUT)
