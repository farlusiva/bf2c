# bf2c


## About

bf2c is a optimising brainfuck to C compiler written in Python.

## Requirements


Python 3.6 or above

## Usage


The general usage is as follows:

__`make input=FILE`__

If you want to pass options to compile.py, for example using compiling with
bignum cellsizes, you can do this:

__`make input=FILE args="-o 0"`__

Since the makefile uses the CFLAGS variable for the C flags, you can also
manually set it too. The same goes for all other variables used in the makefile.  
For example, if you want to compile bf to unoptimised C with 16-bit cells,
to then compile it to unoptimised x86 with clang, you can do this:

__`make input=FILE args="--slow -c 16" CFLAGS="-O0 -m32" CC="clang"`__
